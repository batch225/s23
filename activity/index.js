// s23 Activity Codes:

let trainer = {};

trainer.name = "Ash Ketchun";
trainer.age = 10;
trainer.pokemon  = ["Arceus",  "Giratina",  "Dialga",  "Palkia"];
trainer.friends = {
  Hoenn: ["May", "Max"],
  Kanto: ["Brock","Misty"]
}
console.log(trainer);
console.log("Result of dot notation");
console.log(`Trainer's name: ${trainer.name}`);
console.log(`Trainer's age: ${trainer.age}`);
console.log("Result of square bracket notation");
console.log(trainer["pokemon"]);
console.log(`Friends: `);
console.log(`Hoenn: ${trainer.friends.Hoenn[0]}, ${trainer.friends.Hoenn[1]}`);
console.log(`Kanto: ${trainer.friends.Kanto[0]}, ${trainer.friends.Kanto[1]}`);

trainer.talk = function talk() {
  console.log("Pikachu! I choose you!");
};

trainer.talk();

function Pokemon (name, level, health, attack) {
  this.name = name;
  this.level = level;
  this.health = 2 * level;
  this.attack = level;

      this.tackle = function(target) {
          console.log(`${this.name} tackled ${target.name}`);
          target.health -= this.attack;
          console.log(`${target.name}'s health is now reduced to ${target.health}`);
          target.health <= 0 ? target.faint() : null;
          };
      this.faint = function(){
          console.log(this.name + ' fainted.');
      }
};

let pikachu = new Pokemon("Pikachu", 10);
let charizard = new Pokemon("Charizard", 50);
let bulbasaur = new Pokemon("Bulbasaur", 8);
let squirtle = new Pokemon("Squirtle", 13);

console.log(pikachu);
console.log(charizard);
console.log(bulbasaur);
console.log(squirtle);

pikachu.tackle(charizard);
console.log(charizard);

charizard.tackle(pikachu);
console.log(pikachu);





